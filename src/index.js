/* eslint-disable no-param-reassign */
const Boom = require('@hapi/boom');
const OAuth2Server = require('oauth2-server');

const Request = OAuth2Server.Request;
const Response = OAuth2Server.Response;

function schemeDefault(oauth) {
  return (serv, opts) => {
    return {
      authenticate(req, h) {
        const request = new Request({
          method: req.method.toUpperCase(),
          query: req.query,
          headers: req.headers,
        });
        const response = new Response({});
        return oauth.authenticate(request, response)
          .then(token => {
            return h.authenticated({ credentials: token });
          }).catch(err => {
            return Boom.boomify(err, { statusCode: err.statusCode });
          });
      },
    };
  };
}

function handlerDefault(oauth) {
  return req => {
    req.log('oauth', 'Init log');
    const request = new Request({
      method: req.method.toUpperCase(),
      query: req.query,
      headers: req.headers,
      body: req.payload,
    });
    const response = new Response({});
    return oauth.token(request, response)
      .then(token => {
        return token;
      })
      .catch(err => {
        return Boom.boomify(err, { statusCode: err.statusCode });
      });
  };
}

module.exports = {
  name: 'oauth',
  version: '1.0.0',
  Request,
  Response,
  register: async (server, opts) => {
    const options = {
      schemeName: 'oauthSchema',
      strategyName: 'oauth',
      ...opts,
    };
    const oauth = new OAuth2Server(options);
    await server.auth.scheme(
      options.schemeName,
      (options.scheme && options.scheme instanceof Function)
        ? options.scheme(oauth)
        : schemeDefault(oauth),
    );
    await server.auth.strategy('oauth', options.strategyName);
    server.route({
      method: 'POST',
      path: '/oauth/token',
      handler: (options.handler && options.handler instanceof Function)
        ? options.handler(oauth)
        : handlerDefault(oauth),
    });
    if (options.tokenTestURI) {
      server.route({
        method: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
        path: '/oauth/token/test',
        handler: async request => {
          try {
            const { credentials } = await request.server.auth
              .test('oauth', request);
            request.log('oauth:test', credentials);
            return { status: true, user: credentials.name };
          } catch (err) {
            request.log('oauth:test:error', err);
            return { status: false };
          }
        },
      });
    }
    if (options.tokenVerifyURI) {
      server.route({
        method: 'GET',
        path: '/oauth/token/verify',
        handler: async request => {
          try {
            const credentials = await request.server.auth.verify(request);
            request.log('oauth:verify', credentials);
            return { status: true, user: credentials.name };
          } catch (err) {
            request.log('oauth:verify:error', err);
            return { status: false };
          }
        },
      });
    }
  },
};
