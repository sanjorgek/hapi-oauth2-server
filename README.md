# hapi-oauth2-server

OAuth2-Server plugin for [Hapi.js](https://hapijs.com/tutorials/auth?lang=en_US)

[![npm](https://img.shields.io/npm/v/hapi-oauth2-server.svg)](https://www.npmjs.com/package/hapi-oauth2-server)
[![pipeline status](https://gitlab.com/sanjorgek/hapi-oauth2-server/badges/master/pipeline.svg)](https://gitlab.com/sanjorgek/hapi-oauth2-server/commits/master)
[![coverage report](https://gitlab.com/sanjorgek/hapi-oauth2-server/badges/master/coverage.svg)](https://gitlab.com/sanjorgek/hapi-oauth2-server/commits/master)
[![npm dev dependency version](https://img.shields.io/npm/dependency-version/hapi-oauth2-server/dev/eslint.svg)](sanjorgek.com)
[![Snyk Vulnerabilities for npm package](https://img.shields.io/snyk/vulnerabilities/npm/hapi-oauth2-server.svg)](sanjorgek.com)

## Installation

    $ npm install -s hapi-oauth2-server

## Quick Start

```js
const OAuthPlugin = require('hapi-oauth2-server');

const server = Hapi.server({
  port: 3002,
  host: 'localhost',
});

app = {
  start: async () => {
    await server.register({
      OAuthPlugin,
      options: {
        schemeName: 'test', // Default: oauthSchema
        strategyName: 'test', // Default: oauth
        model: {  // Required
          // Read https://oauth2-server.readthedocs.io/en/latest/model/spec.html
        },
        scheme(oauth) { // Optional
          // Read https://hapijs.com/api#-serverauthschemename-scheme
          return (serv, opts) => {
            // See docs
          };
        },
        handler(oauth) { // Optional
          // Read https://hapijs.com/api#server.auth.strategy()
          return req => {
            // See docs
          };
        },
      },
    });
    await server.start();
  },
  stop: async () => {
    return server.stop({ timeout: 10000 });
  },
  server,
};
await app.start();
```

## Globals

```js
const Request = OAuthPlugin.Request;
const Response = OAuthPlugin.Response;
```
