/* global before after describe it models */
const request = require('request-promises');
const should = require('should');
const assert = require('assert');
const Hapi = require('@hapi/hapi');
const Boom = require('@hapi/boom');

const plugin = require('../src/index');

const preResponse = (req, h) => {
  const response = req.response;
  if (!response.isBoom) {
    return h.continue;
  }
  return response;
};

/* istanbul ignore next */
process.on('unhandledRejection', err => {
  process.exit(1);
});

const sleep = ms => {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const config = {
	clients: [{
		id: 'application',	// TODO: Needed by refresh_token grant, because there is a bug at line 103 in https://github.com/oauthjs/node-oauth2-server/blob/v3.0.1/lib/grant-types/refresh-token-grant-type.js (used client.id instead of client.clientId)
		clientId: 'application',
		clientSecret: 'secret',
		grants: [
			'password',
			'refresh_token'
		],
		redirectUris: []
	}],
	confidentialClients: [{
		clientId: 'confidentialApplication',
		clientSecret: 'topSecret',
		grants: [
			'password',
			'client_credentials'
		],
		redirectUris: []
	}],
	tokens: [],
	users: [{
		username: 'pedroetb',
		password: 'password'
	}]
};

let app;

describe('Create server', () => {
  before(async () => {
    const server = Hapi.server({
      port: 3000,
      host: 'localhost',
    });
    
    app = {
      start: async () => {
        await server.ext('onPreResponse', preResponse);
        await server.register({
          plugin,
          options: {
            schemeName: 'test',
            strategyName: 'test',
            model: {},
          },
        });
        await server.start();
      },
      stop: async () => {
        return server.stop({ timeout: 10000 });
      },
      server,
    };

    await app.start();
  });
  it('should get main page', () => {
    return request({
      uri: `http://localhost:3000/`,
      json: true,
    })
      .then(res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(404);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.have.property('statusCode');
        body.should.have.property('error');
        body.should.have.property('message');
      });
  });
});

describe('Test handler and scheme', () => {
  before(async () => {
    const server = Hapi.server({
      port: 3002,
      host: 'localhost',
    });
  
    app = {
      start: async () => {
        await server.ext('onPreResponse', preResponse);
        await server.register({
          plugin,
          options: {
            schemeName: 'test',
            strategyName: 'test',
            model: {
              getAccessToken(token) {
                const tokens = config.tokens.filter(function(savedToken) {
                  return savedToken.accessToken === token;
                });
                return tokens[0];
              },
              getClient(clientId, clientSecret) {
                const clients = config.clients.filter(function(client) {
                  return client.clientId === clientId && client.clientSecret === clientSecret;
                });
                const confidentialClients = config.confidentialClients.filter(function(client) {
                  return client.clientId === clientId && client.clientSecret === clientSecret;
                });
                return clients[0] || confidentialClients[0];
              },
              saveToken(token, client, user) {
                token.client = {
                  id: client.clientId
                };
                token.user = {
                  id: user.username || user.clientId
                };
                config.tokens.push(token);
                return token;
              },
              getUser(username, password) {
                const users = config.users.filter(function(user) {
                  return user.username === username && user.password === password;
                });
                return users[0];
              },
              getUserFromClient(client) {
                const clients = config.confidentialClients.filter(function(savedClient) {
                  return savedClient.clientId === client.clientId && savedClient.clientSecret === client.clientSecret;
                });
                return clients[0];
              },
              getRefreshToken(refreshToken) {
                const tokens = config.tokens.filter(function(savedToken) {
                  return savedToken.refreshToken === refreshToken;
                });
                if (!tokens.length) {
                  return;
                }
                const token = Object.assign({}, tokens[0]);
                token.user.username = token.user.id;
                return token;
              },
              revokeToken(token) {
                config.tokens = config.tokens.filter(function(savedToken) {
                  return savedToken.refreshToken !== token.refreshToken;
                });
                const revokedTokensFound = config.tokens.filter(function(savedToken) {
                  return savedToken.refreshToken === token.refreshToken;
                });
                return !revokedTokensFound.length;
              },
            },
            scheme(oauth) {
              return (serv, opts) => {
                return {
                  authenticate(req, h) {
                    return Boom.badRequest('Error');
                  },
                };
              };
            },
            handler(oauth) {
              return req => {
                return Boom.badRequest('Error');
              };
            },
          },
        });
        await server.start();
      },
      stop: async () => {
        return server.stop({ timeout: 10000 });
      },
      server,
    };
    await app.start();
  });

  it('should get main page', () => {
    return request({
      uri: `http://localhost:3002/`,
      json: true,
    })
      .then(res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(404);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.have.property('statusCode');
        body.should.have.property('error');
        body.should.have.property('message');
      });
  });

  it('should refuse', () => {
    return request({
      method: 'POST',
      uri: `http://localhost:3002/oauth/token`,
      json: true,
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(400);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.message.should.be.equals('Error');
      });
  });
});

describe('Test models', () => {
  before(async () => {
    const server = Hapi.server({
      port: 3001,
      host: 'localhost',
    });
    
    app = {
      start: async () => {
        await server.ext('onPreResponse', preResponse);
        await server.register({
          plugin,
          options: {
            schemeName: 'test',
            strategyName: 'test',
            accessTokenLifetime: 4,
            refreshTokenLifetime: 6,
            authorizationCodeLifetime: 3,
            tokenVerifyURI: true,
            tokenTestURI: true,
            model: {
              getAccessToken(token) {
                const tokens = config.tokens.filter(function(savedToken) {
                  return savedToken.accessToken === token;
                });
                return tokens[0];
              },
              getClient(clientId, clientSecret) {
                const clients = config.clients.filter(function(client) {
                  return client.clientId === clientId && client.clientSecret === clientSecret;
                });
                const confidentialClients = config.confidentialClients.filter(function(client) {
                  return client.clientId === clientId && client.clientSecret === clientSecret;
                });
                return clients[0] || confidentialClients[0];
              },
              saveToken(token, client, user) {
                token.client = {
                  id: client.clientId
                };
                token.user = {
                  id: user.username || user.clientId
                };
                config.tokens.push(token);
                return token;
              },
              getUser(username, password) {
                const users = config.users.filter(function(user) {
                  return user.username === username && user.password === password;
                });
                return users[0];
              },
              getUserFromClient(client) {
                const clients = config.confidentialClients.filter(function(savedClient) {
                  return savedClient.clientId === client.clientId && savedClient.clientSecret === client.clientSecret;
                });
                return clients[0];
              },
              getRefreshToken(refreshToken) {
                const tokens = config.tokens.filter(function(savedToken) {
                  return savedToken.refreshToken === refreshToken;
                });
                if (!tokens.length) {
                  return;
                }
                const token = Object.assign({}, tokens[0]);
                token.user.username = token.user.id;
                return token;
              },
              revokeToken(token) {
                config.tokens = config.tokens.filter(function(savedToken) {
                  return savedToken.refreshToken !== token.refreshToken;
                });
                const revokedTokensFound = config.tokens.filter(function(savedToken) {
                  return savedToken.refreshToken === token.refreshToken;
                });
                return !revokedTokensFound.length;
              },
            },
          },
        });
        await server.start();
      },
      stop: async () => {
        return server.stop({ timeout: 10000 });
      },
      server,
    };
    return app.start();
  });

  it('should get main page', () => {
    return request({
      uri: `http://localhost:3001/`,
      json: true,
    })
      .then(res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(404);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.have.property('statusCode');
        body.should.have.property('error');
        body.should.have.property('message');
      });
  });

  it('should refuse wrong content-type', () => {
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      json: true,
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(400);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.message.should.be.equals('Invalid request: content must be application/x-www-form-urlencoded');
      });
  });

  it('should refuse missing client credentials', () => {
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(400);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.message.should.be.equals('Invalid client: cannot retrieve client credentials');
      });
  });

  it('should refuse missing grant type', () => {
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(400);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.message.should.be.equals('Missing parameter: `grant_type`');
      });
  });

  it('should refuse missing username', () => {
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "password",
      }
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(400);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.message.should.be.equals('Missing parameter: `username`');
      });
  });

  it('should refuse with missing refresh_token', () => {
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "refresh_token",
      }
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(400);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.message.should.be.equals('Missing parameter: `refresh_token`');
      });
  });

  it('should refuse missing password', () => {
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "password",
        username: "user",
      }
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(400);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.message.should.be.equals('Missing parameter: `password`');
      });
  });

  it('should not auth with invalid credentials 1', () => {
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "password",
        username: "user",
        password: "password",
      },
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(400);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.message.should.be.equals('Invalid grant: user credentials are invalid');
      });
  });

  it('should not auth with invalid credentials 2', () => {
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "password",
        username: "pedroetb",
        password: "wfdwegfegf",
      },
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(400);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.message.should.be.equals('Invalid grant: user credentials are invalid');
      });
  });

  it('should not auth with invalid credentials 3', () => {
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic algo',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "password",
        username: "pedroetb",
        password: "password",
      },
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(400);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.message.should.be.equals('Invalid client: cannot retrieve client credentials');
      });
  });

  it('should not auth with invalid credentials 4', () => {
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic '+Buffer.from('application:secreto').toString('base64'),
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "password",
        username: "test@mail.com",
        password: "password",
      },
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(401);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.message.should.be.equals('Invalid client: client is invalid');
      });
  });

  it('should not auth with invalid credentials 5', () => {
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic '+Buffer.from('applicacion:secret').toString('base64'),
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "password",
        username: "test@mail.com",
        password: "password",
      },
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(401);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.message.should.be.equals('Invalid client: client is invalid');
      });
  });

  it('should not auth with invalid credentials 6', () => {
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic '+Buffer.from('application:secreto').toString('base64'),
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "password",
        username: "test@mail.com",
        password: "password",
      },
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(401);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.message.should.be.equals('Invalid client: client is invalid');
      });
  });

  it('should not auth with invalid credentials 7', () => {
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic '+Buffer.from('application:secret').toString('base64'),
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "password",
        username: "test@mail.com",
        password: "contraseña",
      },
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(400);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.message.should.be.equals('Invalid grant: user credentials are invalid');
      });
  });

  it('should refuse with invalid refresh_token', () => {
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "refresh_token",
        refresh_token: 'algo',
      }
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(400);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.message.should.be.equals('Invalid grant: refresh token is invalid');
      });
  });

  it('should create a new token with password grant', async () => {
    const token = await request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "password",
        username: "pedroetb",
        password: "password",
      },
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(200);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('accessToken', 'accessTokenExpiresAt', 'refreshToken', 'refreshTokenExpiresAt');
        body.should.have.properties('client');
        body.should.have.properties('user');
        body.should.not.have.properties('_id');
        body.should.not.have.properties('createdAt');
        body.should.not.have.properties('updatedAt');
        return res.body;
      });
    return request({
      method: 'POST',
      uri: 'http://localhost:3001/oauth/token',
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "refresh_token",
        refresh_token: token.refreshToken,
      },
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(200);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('accessToken', 'accessTokenExpiresAt', 'refreshToken', 'refreshTokenExpiresAt');
        body.should.have.properties('client');
        body.should.have.properties('user');
        body.should.not.have.properties('_id');
        body.should.not.have.properties('createdAt');
        body.should.not.have.properties('updatedAt');
        body.accessToken.should.be.not.equals(token.accessToken);
      });
  });

  it('should deactivate old tokens', async () => {
    const token = await request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "password",
        username: "pedroetb",
        password: "password",
      },
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(200);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('accessToken', 'accessTokenExpiresAt', 'refreshToken', 'refreshTokenExpiresAt');
        body.should.have.properties('client');
        body.should.have.properties('user');
        body.should.not.have.properties('_id');
        body.should.not.have.properties('createdAt');
        body.should.not.have.properties('updatedAt');
        return res.body;
      });
    await request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "refresh_token",
        refresh_token: token.refreshToken,
      },
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(200);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('accessToken', 'accessTokenExpiresAt', 'refreshToken', 'refreshTokenExpiresAt');
        body.should.have.properties('client');
        body.should.have.properties('user');
        body.should.not.have.properties('_id');
        body.should.not.have.properties('createdAt');
        body.should.not.have.properties('updatedAt');
        body.accessToken.should.be.not.equals(token.accessToken);
        body.refreshToken.should.be.not.equals(token.refreshToken);
      });
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "refresh_token",
        refresh_token: token.refreshToken,
      },
    })
      .then(async res => {
        res.should.have.key('body');
        const body = res.body;
        res.statusCode.should.be.equals(400);
        res.headers['content-type'].should.startWith('application/json');
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.error.should.be.equals('Bad Request');
      });

  });

  it('should refuse to secure route', async () => {
    return request({
      method: 'GET',
      uri: `http://localhost:3001/oauth/token/test`,
      headers: {
        authorization: 'Bearer dgerhgetrh',
      },
      json: true,
    })
      .then(async res => {
        res.should.have.key('body');
        const body = res.body;
        res.statusCode.should.be.equals(200);
        res.headers['content-type'].should.startWith('application/json');
        body.should.be.an.Object();
        body.should.have.properties('status');
        body.status.should.be.equals(false);
      });
  });

  it('should access to secure route', async () => {
    const token = await request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "password",
        username: "pedroetb",
        password: "password",
      },
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(200);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('accessToken', 'accessTokenExpiresAt', 'refreshToken', 'refreshTokenExpiresAt');
        body.should.have.properties('client');
        body.should.have.properties('user');
        body.should.not.have.properties('_id');
        body.should.not.have.properties('createdAt');
        body.should.not.have.properties('updatedAt');
        return res.body;
      });
    return request({
      method: 'GET',
      uri: `http://localhost:3001/oauth/token/test`,
      headers: {
        authorization: 'Bearer '+token.accessToken,
      },
      json: true,
    })
      .then(async res => {
        res.should.have.key('body');
        const body = res.body;
        res.statusCode.should.be.equals(200);
        res.headers['content-type'].should.startWith('application/json');
        body.should.be.an.Object();
        body.should.have.properties('status');
        body.status.should.be.equals(true);
      });
  });

  it('should refuse a expired access token', async () => {
    const token = await request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "password",
        username: "pedroetb",
        password: "password",
      },
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(200);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('accessToken', 'accessTokenExpiresAt', 'refreshToken', 'refreshTokenExpiresAt');
        body.should.have.properties('client');
        body.should.have.properties('user');
        body.should.not.have.properties('_id');
        body.should.not.have.properties('createdAt');
        body.should.not.have.properties('updatedAt');
        return res.body;
      });
    await sleep(6000);
    await request({
      method: 'GET',
      uri: `http://localhost:3001/oauth/token/test`,
      headers: {
        authorization: 'Bearer '+token.access_token,
      },
      json: true,
    })
      .then(async res => {
        res.should.have.key('body');
        const body = res.body;
        res.statusCode.should.be.equals(200);
        res.headers['content-type'].should.startWith('application/json');
        body.should.be.an.Object();
        body.should.have.properties('status');
        body.status.should.be.equals(false);
      });
    return request({
      method: 'GET',
      uri: `http://localhost:3001/oauth/token/verify`,
      headers: {
        authorization: 'Bearer '+token.access_token,
      },
      json: true,
    })
      .then(async res => {
        res.should.have.key('body');
        const body = res.body;
        res.statusCode.should.be.equals(200);
        res.headers['content-type'].should.startWith('application/json');
        body.should.be.an.Object();
        body.should.have.properties('status');
        body.status.should.be.equals(false);
      });
  });

  it('should refuse access token and refresh', async () => {
    const token = await request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "password",
        username: "pedroetb",
        password: "password",
      },
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(200);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('accessToken', 'accessTokenExpiresAt', 'refreshToken', 'refreshTokenExpiresAt');
        body.should.have.properties('client');
        body.should.have.properties('user');
        body.should.not.have.properties('_id');
        body.should.not.have.properties('createdAt');
        body.should.not.have.properties('updatedAt');
        return res.body;
      });
    await sleep(4000);
    await request({
      method: 'GET',
      uri: `http://localhost:3001/oauth/token/test`,
      headers: {
        authorization: 'Bearer '+token.access_token,
      },
      json: true,
    })
      .then(async res => {
        res.should.have.key('body');
        const body = res.body;
        res.statusCode.should.be.equals(200);
        res.headers['content-type'].should.startWith('application/json');
        body.should.be.an.Object();
        body.should.have.properties('status');
        body.status.should.be.equals(false);
      });
    await request({
      method: 'GET',
      uri: `http://localhost:3001/oauth/token/verify`,
      headers: {
        authorization: 'Bearer '+token.access_token,
      },
      json: true,
    })
      .then(async res => {
        res.should.have.key('body');
        const body = res.body;
        res.statusCode.should.be.equals(200);
        res.headers['content-type'].should.startWith('application/json');
        body.should.be.an.Object();
        body.should.have.properties('status');
        body.status.should.be.equals(false);
      });
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "refresh_token",
        refresh_token: token.refreshToken,
      },
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(200);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('accessToken', 'accessTokenExpiresAt', 'refreshToken', 'refreshTokenExpiresAt');
        body.should.have.properties('client');
        body.should.have.properties('user');
        body.should.not.have.properties('_id');
        body.should.not.have.properties('createdAt');
        body.should.not.have.properties('updatedAt');
        body.refreshToken.should.not.be.equals(token.refreshToken);
      });
  });

  it('should refuse a expired refresh token', async () => {
    const token = await request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "password",
        username: "pedroetb",
        password: "password",
      },
    })
      .then(async res => {
        res.should.have.key('body');
        res.statusCode.should.be.equals(200);
        res.headers['content-type'].should.startWith('application/json');
        const body = res.body;
        body.should.be.an.Object();
        body.should.have.properties('accessToken', 'accessTokenExpiresAt', 'refreshToken', 'refreshTokenExpiresAt');
        body.should.have.properties('client');
        body.should.have.properties('user');
        body.should.not.have.properties('_id');
        body.should.not.have.properties('createdAt');
        body.should.not.have.properties('updatedAt');
        return res.body;
      });
    await sleep(6000);
    return request({
      method: 'POST',
      uri: `http://localhost:3001/oauth/token`,
      headers: {
        authorization: 'Basic YXBwbGljYXRpb246c2VjcmV0',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      json: true,
      form: {
        grant_type: "refresh_token",
        refresh_token: token.refreshToken,
      },
    })
      .then(async res => {
        res.should.have.key('body');
        const body = res.body;
        res.statusCode.should.be.equals(400);
        res.headers['content-type'].should.startWith('application/json');
        body.should.be.an.Object();
        body.should.have.properties('statusCode', 'error', 'message');
        body.error.should.be.equals('Bad Request');
        body.message.should.be.equals('Invalid grant: refresh token has expired');
      });
  });
});

after(async () => {
  return app.stop();
});
