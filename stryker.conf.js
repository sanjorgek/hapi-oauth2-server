module.exports = config => {
  config.set({
    mutator: 'javascript',
    packageManager: 'npm',
    reporters: ['html', 'clear-text'],
    testRunner: 'mocha',
    transpilers: [],
    testFramework: 'mocha',
    mochaOptions: {
      spec: ['test/**/*.js'],
    },
    coverageAnalysis: 'all',
    mutate: ['src/**/*.js'],
    thresholds: { high: 100, low: 80, break: 59 },
  });
};
